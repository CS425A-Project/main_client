import transaction
from ZODB import FileStorage, DB
import sys
import os
import file_uploader.file_uploader as fu
import ftplib
import json
import socket
import datetime
import getpass
import hashlib
from subprocess import call

database_file_path = 'database_files/filetable_main.db'

def get_hash(string):
	hash_object = hashlib.sha256(string.encode())
	hex_dig = hash_object.hexdigest()
	return hex_dig

def get_ipv4_addr(host, port):
    addr = socket.getaddrinfo(host, port, socket.AF_INET, 0, socket.SOL_TCP)
    if len(addr) == 0:
    	raise Exception("There is no IPv4 address configured for host: "+host)
    return addr[0][-1]

def send_data(data, host, port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(get_ipv4_addr(host, port=port))

	s.send(data.encode())
	response = s.recv(1024)
	s.close()
	return json.JSONDecoder().decode(response.decode())

def init_database():
	storage = FileStorage.FileStorage(database_file_path)
	db = DB(storage)
	connection = db.open()
	filetable = connection.root()
	return filetable

def init_filesystem(filetable):
	filetable['users'] = {}
	filetable['auth'] = {}
	filetable._p_changed = 1
	transaction.commit()

def init_user(filetable, username):
	new_directory = {}
	new_directory['.'] =  {'val': new_directory, 'is_file': False}
	filetable['users'][username] = {}
	filetable['users'][username]['root'] = new_directory
	filetable._p_changed = 1
	transaction.commit()


def create_directory(filetable, curr_directory, directory_name):
	if directory_name in curr_directory.keys():
		print('Error: File with name '+directory_name+' already exists.')
		return
	new_directory = {}
	new_directory['.'] =  {'val': new_directory, 'is_file': False}
	new_directory['..'] = {'val': curr_directory, 'is_file': False}
	curr_directory[directory_name] = {'val': new_directory, 'is_file': False}
	filetable._p_changed = 1
	transaction.commit()

def print_ls(curr_directory):
	for key in curr_directory.keys():
		print(key+' ', end="")
	print('')

def print_pwd(curr_path):
	print('/', end="")
	for directory in curr_path:
		print(directory+'/',end="")
	print('')

def check_valid_cd(curr_directory, directory_name):
	if directory_name in curr_directory.keys():
		if not curr_directory[directory_name]['is_file']:
			return True
	else:
		return False


def upload_file(filetable, curr_directory, file_path, encryptionKey, rename=None):
	if not os.path.exists(file_path):
		print("Error: File path is invalid.")
	elif not os.path.isfile(file_path):
		print("Error: File does not exist.")
	elif not os.access(file_path, os.R_OK):
		print("Error: File cannot be read.")
	else:
		file_name = ''
		if rename == None:
			file_name = os.path.basename(file_path)
		else:
			file_name = rename
		if file_name in curr_directory.keys():
			print('Error: File with this name already exists.')
		else:
			temp_path = '/tmp/botnet_crypted'
			call(["aescrypt", "-e", "-p", password, "-o", temp_path, file_path])
			init_data_node = fu.upload_file(temp_path)
			curr_directory[file_name] = {'val': init_data_node['filename'], 'ip': get_tuples(init_data_node['ip'], init_data_node['port']), 'is_file': True}
			filetable._p_changed = 1
			transaction.commit()

def del_file(curr_directory, filename):
	if filename not in curr_directory.keys():
		print("Error: file does not exist.")
		return
	data = {
			'val': curr_directory[filename]['val'],
			'ip': curr_directory[filename]['ip'],
			}
	while data['val'] != None:
		encoded_data = json.JSONEncoder().encode(
				{
				'type':'delete',
				'filename': data['val']
				}
				)
		ftp = ftplib.FTP()
		print(data)
		error = True
		for ip_port in data['ip']:
			try:
				ftp.connect(ip_port[0])
				ftp.login('botnet', 'botnet')
				ftp.delete(data['val'])
				ftp.close()
				data = send_data(encoded_data, ip_port[0], ip_port[1])
				error = False
			except:
				pass
		if error:
			print("Error Deleting file.")
			return
	curr_directory.pop(filename, None)

def get_tuples(ip_array, port):
	return [(ip, port) for ip in ip_array]

def retreive_file(curr_directory, file_name, file_path, encryptionKey):
	# chunk_list = []
	temp_path = '/tmp/botnet_decrypted'
	f = open(temp_path, 'wb+')
	data = {
			'val': curr_directory[file_name]['val'],
			'ip': curr_directory[file_name]['ip'],
			}
	while data['val'] != None:
		encoded_data = json.JSONEncoder().encode(
				{
				'type':'retreive',
				'filename': data['val']
				}
				)
		ftp = ftplib.FTP()
		print(data)
		error = True
		for ip_port in data['ip']:
			try:
				ftp.connect(ip_port[0])
				ftp.login('botnet', 'botnet')
				ftp.retrbinary('RETR '+ data['val'], f.write)
				ftp.close()
				data = send_data(encoded_data, ip_port[0], ip_port[1])
				error = False
				break
			except:
				pass
		if error:
			print("Error retreiving file.")
			return
	f.close()
	call(["aescrypt", "-d", "-p", password, "-o", file_path, temp_path])


def tree(filetable, username, num_tabs=1):
	if num_tabs == 1:
		print('/users/'+username+'/root')
	seperation = "".join(['|---']*num_tabs)
	for key in filetable.keys():
		if key != '.' and key != '..':
			to_print = seperation+key
			if filetable[key]['is_file']:
				to_print = to_print+'('+filetable[key]['val']+')'
			print(to_print)
			if not filetable[key]['is_file']:
				tree(filetable[key]['val'], None, num_tabs+1)

def insert_user(filetable, username, pass_hash):
	if username in filetable['auth'].keys():
		print("Error: Useralready exists.")
		exit(0)
	else:
		filetable['auth'][username] = pass_hash
	init_user(filetable, username)
	filetable._p_changed = 1
	transaction.commit()

def validate(filetable, user, passwd):
	if user in filetable['auth'].keys():
		if get_hash(passwd) == filetable['auth'][user]:
			return passwd
	print("Error: Authentication failure.")
	exit(0)

if __name__=='__main__':
	curr_directory = None
	password = ''
	username = ''
	fu.init_dm('datanodes_monitor/ip_list.db')
	print('Initializing database...')
	filetable = init_database()
	print('Finished initializing database.\nDatabase files maintained in '+database_file_path)
	if sys.argv[1] == 'init':
		init_filesystem(filetable)
		print('New Username: ',end="")
		username = input()
		password = getpass.getpass()
		insert_user(filetable, username, get_hash(password))
	elif sys.argv[1] == 'restore':
		if sys.argv[2] == 'register':
			print('New Username: ',end="")
			username = input()
			password = getpass.getpass()
			insert_user(filetable, username, get_hash(password))
		elif sys.argv[2] == 'login':
			print('Username: ',end="")
			username = input()
			password = getpass.getpass()
			validate(filetable, username, password)
	else:
		print('Invalid arguments. Exiting.')
		exit(0)
	curr_path = ['users', username, 'root']
	curr_directory = filetable['users'][username]['root']
	while(True):
		print('$ ',end="",flush=True)
		instr = input()
		args = instr.split(' ')
		if args[0] == 'mkdir':
			create_directory(filetable, curr_directory, args[1])
		elif args[0] == 'ls':
			print_ls(curr_directory)
		elif args[0] == 'pwd':
			print_pwd(curr_path)
		elif args[0] == 'exit':
			exit(0)
		elif args[0] == 'retreive':
			retreive_file(curr_directory, args[1], args[2], password)
		elif args[0] == 'upload':
			if len(args) == 3:
				upload_file(filetable, curr_directory, args[1], password, args[2])
			else:
				upload_file(filetable, curr_directory, args[1], password)
		elif args[0] == 'rm':
			del_file(curr_directory, args[1])
		elif args[0] == 'tree':
			tree(filetable['users'][username]['root'], username)
		elif args[0] == 'cd':
			if check_valid_cd(curr_directory, args[1]):
				curr_directory = curr_directory[args[1]]['val']
				if args[1] == '..':
					curr_path = curr_path[:-1]
				elif args[1] != '.':
					curr_path.append(args[1])
			else:
				print('Directory does not exists.')
		else:
			print('Command not found: ' + args[0])